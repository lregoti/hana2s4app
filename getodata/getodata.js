"use strict";
var express = require("express");
var request = require("request");

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

module.exports = function(){
	var app = express(); 
	
// /getodata/ Router
	// 
	app.route("/")	// /getodata/
	  .get(function(req, res) {
	    res.send("Hello Get ODATA World Node.js");
	});
	
	// Note the for this service URL detail discovery to work you must define a user provided service
	// xs cups sapcp-http-odata -p '{"url":"https://sfp00maind0cd77f9b.us2.hana.ondemand.com/http/odata/http.xsodata/","type":"SAPCP"}'

	app.route("/get")	// /getodata/get
	  .get(function(req, res) {
		// var svcs = JSON.parse(process.env.VCAP_SERVICES);
		// var creds = svcs["user-provided"][0].credentials;
		// var url = creds.url;
		var url = "https://vhcals4hci.dummy.nodomain:44300/sap/opu/odata/sap/FDP_V3_BD_LIST_STANDARD_SRV/BillingDocument/?$format=json&$top=5";
		var username = "BPINST";
		var password = "Welcome1";
		var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");

		console.log("HTTP ODATA URL = " + url + "");

		request({
			url: url,
			method: "GET",
			headers: {
				"Authorization" : auth,
				"Accept": "application/json"
			}
		}, function(error, response, body) {
			console.log("LR Body:\n" + body + "\n");
			if (response.statusCode == 200) {
				//console.log("Body:\n" + body + "\n");
				var jb = JSON.parse(body);
				if (jb.d.results.length > 0) {
					for(var i=0; i < jb.d.results.length; i++) {
						var bdoc = jb.d.results[i];
						console.log("BillingDocument: " + bdoc.BillingDocument +"\n");
						console.log("TotalNetAmount: " + bdoc.TotalNetAmount +"\n");
						console.log("SalesOrganization: " + bdoc.SalesOrganization +"\n");
					}
				} else {
					console.log("No Connection Data");
				}
			} else {
				console.log(error);
			}

		});

	    res.send("Get ODATA URL : " + url);
	});
	

	//Simple Database Select - Async Waterfall
	app.route("/dummy2")	// /getodata/dummy2
		.get(function(req, res) {
			var client = req.db;
			async.waterfall([
				function prepare(callback) {
					client.prepare("select SESSION_USER from \"mta_nodejs.db::Dummy\" ",
						function(err, statement) {
							callback(null, err, statement);
						});
				},
				function execute(err, statement, callback) {
					statement.exec([], function(execErr, results) {
						callback(null, execErr, results);
					});
				},
				function response(err, results, callback) {
					if (err) {
						res.type("text/plain").status(500).send("ERROR: " + err);
					} else {
						var result = JSON.stringify({
							Objects: results
						});
						res.type("application/json").status(200).send(result);
					}
					callback();
				}
			]);
		});

	
   return app;	
};