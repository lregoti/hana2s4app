/*eslint no-console: 0*/
"use strict";

var xsjs  = require("sap-xsjs");
var xsenv = require("sap-xsenv");

var port  = process.env.PORT || 3000;

var server = require('http').createServer();
var express = require("express");
var getodata = require("./getodata"); 

//Create base Express Server App
var app = express(); 
app.use("/getodata", getodata());

var options = {
	anonymous : true, // remove to authenticate calls
	redirectUrl : "/getodata/get"
};

// configure HANA
try {
	options = Object.assign(options, xsenv.getServices({ hana: {tag: "hana"} }));
} catch (err) {
	console.log("[WARN]", err.message);
}

// configure UAA
try {
	options = Object.assign(options, xsenv.getServices({ uaa: {tag: "xsuaa"} }));
} catch (err) {
	console.log("[WARN]", err.message);
}
//Add XSJS to the base app
var xsjsApp = xsjs(options);
app.use(xsjsApp);
server.on('request', app);
server.listen(port, function () {
    console.log('HTTP Server: ' + server.address().port );
});
